#ifndef POINTCLOUD_MERGER_HPP
#define POINTCLOUD_MERGER_HPP

#include <vector>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PCLPointCloud2.h>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>

class PointCloudMerger : public rclcpp::Node
{
public:
  PointCloudMerger();

private:
  void livoxCallback(const sensor_msgs::msg::PointCloud2::ConstSharedPtr& cloud_msg);
  void zedCallback(const sensor_msgs::msg::PointCloud2::ConstSharedPtr& cloud_msg);
  void publishMergedCloud();

  rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr livox_sub_;
  rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr zed_sub_;
  rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr merged_pub_;
  std::vector<sensor_msgs::msg::PointCloud2::ConstSharedPtr> livox_clouds_;
  std::vector<sensor_msgs::msg::PointCloud2::ConstSharedPtr> zed_clouds_;
};

#endif // POINTCLOUD_MERGER_HPP

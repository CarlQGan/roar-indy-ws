#!/usr/bin/env python3

import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration

def generate_launch_description():

    # Set the path to the pointcloud_merger package
    pointcloud_merger_path = get_package_share_directory('pointcloud_merger')

    # Set the name of the topic where the merged point cloud will be published
    merged_topic_name = LaunchConfiguration('merged_topic_name', default='merged_pointcloud')

    # Declare the launch arguments
    merged_topic_arg = DeclareLaunchArgument('merged_topic_name',
                                              default_value=merged_topic_name.default_value,
                                              description='Name of the topic where the merged point cloud will be published')

    # Create a node for the pointcloud_merger
    pointcloud_merger_node = Node(package='pointcloud_merger',
                                  executable='pointcloud_merger_node',
                                  name='pointcloud_merger',
                                  output='screen',
                                  parameters=[{'merged_topic_name': merged_topic_name}])

    # Create a node for the RViz visualization
    rviz_node = Node(package='rviz2',
                     executable='rviz2',
                     name='rviz2',
                     arguments=['-d', os.path.join(pointcloud_merger_path, 'config', 'pointcloud_merger.rviz')])

    # Define the launch description and add the nodes and launch arguments
    ld = LaunchDescription()
    ld.add_action(merged_topic_arg)
    ld.add_action(pointcloud_merger_node)
    ld.add_action(rviz_node)

    return ld

#include <memory>
#include <vector>
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "pcl_conversions/pcl_conversions.h"
#include "pcl_ros/transforms.hpp"

class PointCloudMerger : public rclcpp::Node
{
public:
  PointCloudMerger() : Node("pointcloud_merger")
  {
    // Subscribe to Livox and ZED point cloud topics
    livox_sub_ = this->create_subscription<sensor_msgs::msg::PointCloud2>(
        "livox_pointcloud", 10, std::bind(&PointCloudMerger::livoxCallback, this, std::placeholders::_1));
    zed_sub_ = this->create_subscription<sensor_msgs::msg::PointCloud2>(
        "zed_pointcloud", 10, std::bind(&PointCloudMerger::zedCallback, this, std::placeholders::_1));

    // Advertise merged point cloud topic
    merged_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>("merged_pointcloud", 10);
  }

private:
  void livoxCallback(const sensor_msgs::msg::PointCloud2::SharedPtr msg)
  {
    // Add the Livox point cloud to the list of Livox point clouds
    livox_clouds_.push_back(msg);
    RCLCPP_INFO(this->get_logger(), "Received Livox point cloud with %d points", msg->width * msg->height);

    // Merge the point clouds and publish the result
    publishMergedCloud();
  }

  void zedCallback(const sensor_msgs::msg::PointCloud2::SharedPtr msg)
  {
    // Add the ZED point cloud to the list of ZED point clouds
    zed_clouds_.push_back(msg);
    RCLCPP_INFO(this->get_logger(), "Received ZED point cloud with %d points", msg->width * msg->height);

    // Merge the point clouds and publish the result
    publishMergedCloud();
  }

  void publishMergedCloud()
  {
    // Check if there are at least one Livox and one ZED point cloud
    if (livox_clouds_.empty() || zed_clouds_.empty()) {
      return;
    }

    // Concatenate all Livox point clouds into a single point cloud
    pcl::PCLPointCloud2::Ptr livox_cloud(new pcl::PCLPointCloud2());
    for (const auto& msg : livox_clouds_) {
      pcl::PCLPointCloud2::Ptr tmp_cloud(new pcl::PCLPointCloud2());
      pcl_conversions::toPCL(*msg, *tmp_cloud);
      pcl::concatenate(*tmp_cloud, *livox_cloud, *livox_cloud);
    }

    // Concatenate all ZED point clouds into a single point cloud
    pcl::PCLPointCloud2::Ptr zed_cloud(new pcl::PCLPointCloud2());
    for (const auto& msg : zed_clouds_) {
      pcl::PCLPointCloud2::Ptr tmp_cloud(new pcl::PCLPointCloud2());
      pcl_conversions::toPCL(*msg, *tmp_cloud);
      pcl::concatenate(*tmp_cloud, *zed_cloud, *zed_cloud);
    }

    // Merge the Livox and ZED point clouds
    pcl::PCLPointCloud2::Ptr merged_cloud(new pcl::PCLPointCloud2());
    pcl::concatenate(*livox_cloud, *zed_cloud, *merged_cloud);

    // Convert the merged point cloud to a sensor_msgs::PointCloud2 message and publish it
    sensor_msgs::msg::PointCloud2::SharedPtr merged_msg(new sensor_msgs::msg::PointCloud2());
    pcl_conversions::fromPCL(*merged_cloud, *merged_msg);
    merged_msg->header.stamp = this->get_clock()->now();
    merged_pub_->publish(*merged_msg);

    // Clear the list of Livox and ZED point clouds
    livox_clouds_.clear();
    zed_clouds_.clear();
  }

  rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr livox_sub_;
  rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr zed_sub_;
  rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr merged_pub_;
  std::vector<sensor_msgs::msg::PointCloud2::ConstSharedPtr> livox_clouds_;
  std::vector<sensor_msgs::msg::PointCloud2::ConstSharedPtr> zed_clouds_;
};

int main(int argc, char* argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<PointCloudMerger>());
  rclcpp::shutdown();
  return 0;
}





   
